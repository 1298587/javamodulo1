package com.clientes;

import com.clases.BeneficiosCovid19;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        ArrayList<BeneficiosCovid19> lista1 = new ArrayList<BeneficiosCovid19>();
        ArrayList<BeneficiosCovid19> lista2 = new ArrayList<BeneficiosCovid19>();

        getIdBeneficio(lista1, "Beneficiados que perdieron su trabajo por COVID19", (float) 150000);
        getIdBeneficio(lista1, "Beneficios para persona tercera edad damnificados COVID", (float) 200000);
        getIdBeneficio(lista1, "Beneficios para practicantes sin trabajo por el COVID", (float) 180000);

        getIdBeneficio(lista2, "Beneficiados que perdieron su trabajo por COVID19", (float) 130000);
        getIdBeneficio(lista2, "Beneficios para persona tercera edad damnificados COVID", (float) 250000);
        getIdBeneficio(lista2, "Beneficios para practicantes sin trabajo por el COVID", (float) 190000);

        for (int i = 0; i < lista1.size(); i++) {
            System.out.println("ID: " + lista1.get(i).getId());
            System.out.println("NOMBRE: " + lista1.get(i).getNombre());
            System.out.println("VALOR SUBSIDIO: " + lista1.get(i).getValorSubsidio());
        }
        for (int i = 0; i < lista2.size(); i++) {
            System.out.println("ID: " + lista2.get(i).getId());
            System.out.println("NOMBRE: " + lista2.get(i).getNombre());
            System.out.println("VALOR SUBSIDIO: " + lista2.get(i).getValorSubsidio());
            lista1.add(lista2.get(i));
        }
        idMejorBeneficio(lista1);
    }

    public static void idMejorBeneficio(ArrayList<BeneficiosCovid19> lista1) {
        float variable = 0;
        int idMejor = 0;

        for (int i = 0; i < lista1.size(); i++) {
            if (lista1.get(i).getValorSubsidio() > variable) {
                variable = lista1.get(i).getValorSubsidio();
                idMejor = i;
            }
        }
        System.out.println(" ");
        System.out.println("----------------------------------------------------------");
        System.out.println("EL MEJOR BENEFICIO");
        System.out.println("ID: " + lista1.get(idMejor).getId());
        System.out.println("NOMBRE: " + lista1.get(idMejor).getNombre());
        System.out.println("VALOR SUBSIDIO: " + lista1.get(idMejor).getValorSubsidio());
    }

    private static void getIdBeneficio(ArrayList<BeneficiosCovid19> lista, String nombre, float valor) {
        boolean check = false;
        Integer numero;
        String num = "";
        if (lista.size() > 0) {
            while (check == false) {
                numero = (int) (Math.random() * 10 + 1);
                num = String.valueOf(Integer.valueOf(numero));
                for (int i = 0; i < lista.size(); i++) {
                    if (lista.get(i).getId() != num) {
                        check = true;
                    } else if (check) {
                        check = false;
                    }
                }
            }
        }else{
            numero = (int) (Math.random() * 10 + 1);
            num = String.valueOf(Integer.valueOf(numero));
        }
        lista.add(new BeneficiosCovid19(num, nombre, valor));
    }
}