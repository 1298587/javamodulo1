package clases;

public class CapacidadEndedudamiento {
    Integer ingresosTotales;
    Integer gastosFijos;
    Integer gastoVaribales;
    final double POR_FIJO = 0.35;

    public CapacidadEndedudamiento(Integer ingresosTotales, Integer gastosFijos, Integer gastoVaribales) {
        this.ingresosTotales = ingresosTotales;
        this.gastosFijos = gastosFijos;
        this.gastoVaribales = gastoVaribales;
    }
//Metodos getter and setter para la clase

    //Construye un metodo que retorne una cadena con las propiedades de la clase

    public Integer getIngresosTotales() {
        return ingresosTotales;
    }
    public void setIngresosTotales(Integer ingresosTotales) {
        this.ingresosTotales = ingresosTotales;
    }
    public Integer getGastosFijos() {
        return gastosFijos;
    }
    public void setGastosFijos(Integer gastosFijos) {
        this.gastosFijos = gastosFijos;
    }
    public Integer getGastoVaribales() {
        return gastoVaribales;
    }
    public void setGastoVaribales(Integer gastoVaribales) {
        this.gastoVaribales = gastoVaribales;
    }

    public Double getCapacidadEndeudamiento() {
        // retornar la capacidad de endeudamiento puede ser una cadena con el valor
        double capacidad = (ingresosTotales - (gastosFijos + gastoVaribales)) * POR_FIJO;
        return capacidad;
    }

    public void mostrarCapacidad(){
        double capacidad = getCapacidadEndeudamiento();
        System.out.println(Constants.MSMCAPACIDAD + capacidad);
    }

}