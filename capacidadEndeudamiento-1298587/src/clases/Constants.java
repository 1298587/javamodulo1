package clases;

public class Constants {

    public static final String MSNINGRESO = "Ingrese sus ingresos mensuales:";
    public static final String MSNGASTOFIJO = "Ingrese sus gastos fijos:";
    public static final String MSNGASTOVARIABLE = "Ingrese sus gastos variables:";
    public static final String MSMLETRAS = "EL VALOR NO ES VÁLIDO, CONTIENE LETRAS";
    public static final String MSMCAPACIDAD = "La capacidad de endeudamiento es: ";

    private Constants(){

    }
}
