package com.company;

import clases.CapacidadEndedudamiento;
import clases.Constants;

import java.util.Scanner;

public class Main {
    //Recuerda que aca empieza todo
    public static void main(String[] args) {
        //Con este objeto de la clase Scanner puedes capturar informacion por consola cada ves que lo uses
        // recuerda cerrar el flujo de consulta cada ves lo uses sobre para que los uses in.close()
        int ingMensuales2 = 0;
        int gastosFijos2 = 0;
        int gastosVariables2 = 0;

        Scanner in = new Scanner(System.in);
        String entrada = "SI";

        while (entrada.equals("SI")) {
            System.out.println(Constants.MSNINGRESO);
            String ingMensuales = in.nextLine();
            if (isNumeric(ingMensuales)) {
                ingMensuales2 = Integer.parseInt(ingMensuales);
                entrada = "NO";
            }else{
                System.out.println(Constants.MSMLETRAS);
            }
        }
        entrada = "SI";
        while (entrada.equals("SI")) {
            System.out.println(Constants.MSNGASTOFIJO);
            String gastosFijos = in.nextLine();
            if (isNumeric(gastosFijos)) {
                gastosFijos2 = Integer.parseInt(gastosFijos);
                entrada = "NO";
            }else {
                System.out.println(Constants.MSMLETRAS);
            }
        }
        entrada = "SI";
        while (entrada.equals("SI")){
            System.out.println(Constants.MSNGASTOVARIABLE);
            String gastosVariables = in.nextLine();
            if (isNumeric(gastosVariables)) {
                gastosVariables2 = Integer.parseInt(gastosVariables);
                entrada = "NO";
            }else{
                System.out.println(Constants.MSMLETRAS);
            }
        }

        CapacidadEndedudamiento capEndeudamiento = new CapacidadEndedudamiento(ingMensuales2, gastosFijos2, gastosVariables2);
        capEndeudamiento.getCapacidadEndeudamiento();
        capEndeudamiento.mostrarCapacidad();

    }

    public static boolean isNumeric(String strNum) {
        boolean ret = true;
        try {
            Double.parseDouble(strNum);
        }catch (NumberFormatException e) {
            ret = false;
        }
        return ret;
    }
}